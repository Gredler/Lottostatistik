<?php
define('_JEXEC', 1);
define('JPATH_BASE', '../..');

include_once JPATH_BASE . '/includes/defines.php';
require_once JPATH_BASE . '/includes/framework.php';

require_once dirname(__FILE__) . '/models/Lottery.php';

require_once('controllers/LotteryRESTController.php');

try {
    (new LotteryRESTController())->handleRequest();
} catch (Exception $e) {
    header("HTTP/1.1 400 " . $e->getMessage());
    echo json_encode($e->getMessage());
}
