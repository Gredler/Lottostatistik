<?php
// No direct access
defined('_JEXEC') or die; ?>

<div class="lottery">
    <h2>Lottostatistik</h2>
    <div class="form-inline">
        <div class="form-group">
            <label for="start">Startdatum</label>
            <input type="date" class="form-control" id="start">
            <input type="button" id="btnSearch" class="btn btn-primary" value="anzeigen">
        </div>
    </div>

    <br/>

    <canvas id="lotteryChart" width="400" height="200"></canvas>

    <br />

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Datum</th>
            <th colspan="6">Tipp</th>
        </tr>
        </thead>
        <tbody id="lotteries"></tbody>
    </table>


</div>
