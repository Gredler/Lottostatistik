<?php

require_once 'RESTController.php';
require_once 'models/Lottery.php';

class LotteryRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }

    /**
     * get single/all lotteries or search lotteries
     * all lotteries: GET api.php?r=lotteries
     * single lotteries: GET api.php?r=lotteries/25 -> args[0] = 25
     * filter lotteries: GET api.php?r=lotteries/start/2015-03-04 -> verb = filter, args[0] = 2015-03-04
     */
    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 0) {
            $model = Lottery::getAll();
            $this->response($model);
        } else if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Lottery::get($this->args[0]);
            $this->response($model);
        } else if ($this->verb == 'start' && sizeof($this->args) == 1) {
            $model = Lottery::getAll($this->args[0]);
            $this->response($model);
        } else {
            $this->response("Bad request", 400);
        }
    }

    private function handlePOSTRequest()
    {
	    $model = new Lottery();
	    $model->date = isset($this->file['date']) ? $this->file['date'] : null;
	    $model->num1 = isset($this->file['num1']) ? $this->file['num1'] : null;
	    $model->num2 = isset($this->file['num2']) ? $this->file['num2'] : null;
	    $model->num3 = isset($this->file['num3']) ? $this->file['num3'] : null;
	    $model->num4 = isset($this->file['num4']) ? $this->file['num4'] : null;
	    $model->num5 = isset($this->file['num5']) ? $this->file['num5'] : null;
	    $model->num6 = isset($this->file['num6']) ? $this->file['num6'] : null;

	    if ($model->save()) {
		    $this->response("OK", 201);
	    } else {
		    $this->response($model->errors, 400);
	    }
    }

    private function handlePUTRequest()
    {
	    if ($this->verb == null && sizeof($this->args) == 1) {

		    $model = Lottery::get($this->args[0]);

		    if ($model == null) {
			    $this->response("Not Found", 404);
		    }
		    else {
			    $model->date = isset($this->file['date']) ? $this->file['date'] : $model->date;
			    $model->num1 = isset($this->file['num1']) ? $this->file['num1'] : $model->num1;
			    $model->num2 = isset($this->file['num2']) ? $this->file['num2'] : $model->num2;
			    $model->num3 = isset($this->file['num3']) ? $this->file['num3'] : $model->num3;
			    $model->num4 = isset($this->file['num4']) ? $this->file['num4'] : $model->num4;
			    $model->num5 = isset($this->file['num5']) ? $this->file['num5'] : $model->num5;
			    $model->num6 = isset($this->file['num6']) ? $this->file['num6'] : $model->num6;

			    if ($model->save()) {
				    $this->response("OK", 200);
			    }
			    else {
				    $this->response($model->errors, 400);
			    }
		    }

	    } else {
		    $this->response("Not Found", 404);
	    }
    }

	/**
	 * delete tipp: DELETE api.php?r=lotteries/25 -> args[0] = 25
	 */
    private function handleDELETERequest()
    {
	    if ($this->verb == null && sizeof($this->args) == 1) {
		    Lottery::delete($this->args[0]);
		    $this->response("OK", 200);
	    } else {
		    $this->response("Not Found", 404);
	    }
    }

}