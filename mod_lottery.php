<?php

// No direct access
defined('_JEXEC') or die;

$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_lottery/css/mod_lottery.css');
$document->addScript('modules/mod_lottery/js/jquery.min.js');
$document->addScript('modules/mod_lottery/js/Chart.bundle.min.js');
$document->addScript('modules/mod_lottery/js/mod_lottery.js');

require JModuleHelper::getLayoutPath('mod_lottery');