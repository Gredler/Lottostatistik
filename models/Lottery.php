<?php

require_once 'DatabaseObject.php';

class Lottery implements DatabaseObject, JsonSerializable
{
	private $id;
	private $date;
	private $num1;
	private $num2;
	private $num3;
	private $num4;
	private $num5;
	private $num6;

	private $errors;

	/**
	 * Lottery constructor.
	 *
	 * @param $id
	 * @param $date
	 * @param $num1
	 * @param $num2
	 * @param $num3
	 * @param $num4
	 * @param $num5
	 * @param $num6
	 */
	public function __construct($id = null, $date = null, $num1 = null, $num2 = null, $num3 = null, $num4 = null, $num5 = null, $num6 = null)
	{
		$this->id   = $id;
		$this->date = $date;
		$this->num1 = $num1;
		$this->num2 = $num2;
		$this->num3 = $num3;
		$this->num4 = $num4;
		$this->num5 = $num5;
		$this->num6 = $num6;

		$this->errors = [];
	}


	public function validate()
	{
		return $this->validateNumberValueHelper("Number 1", "num1", $this->num1, 45, 1) &
		$this->validateNumberValueHelper("Number 2", "num2", $this->num2, 45, 1) &
		$this->validateNumberValueHelper("Number 3", "num3", $this->num3, 45, 1) &
		$this->validateNumberValueHelper("Number 4", "num4", $this->num4, 45, 1) &
		$this->validateNumberValueHelper("Number 5", "num5", $this->num5, 45, 1) &
		$this->validateNumberValueHelper("Number 6", "num6", $this->num6, 45, 1) &
		$this->validateDate();
	}

	/**
	 * Validates Value of a number.
	 * Sets/unsets error variable in case of invalid/valid data
	 * @param $label name of the input field displayed in error message
	 * @param $key position in errors array
	 * @param $value the acutal value to be validated
	 * @param $maxValue the allowed maximum value
	 * @param $minValue the required minimum value
	 * @return bool true, if value meets parameters, else false
	 */
	private function validateNumberValueHelper($label, $key, $value, $maxValue, $minValue)
	{
		if ($value > $maxValue) {
			$this->errors[$key] = "$label too big, only values between. $minValue and $maxValue allowed";
			return false;
		} else if ($value < $minValue) {
			$this->errors[$key] = "$label too small, only values between. $minValue and $maxValue allowed.";
			return false;
		} else {
			unset($this->errors[$key]);
			return true;
		}
	}

	/**
	 * Validates the date of this object.
	 * @return bool
	 */
	private function validateDate()
	{
		if ($this->date != null)
		{
			if (!DateTime::createFromFormat('Y-m-d', $this->date)) {
				$this->errors['date'] = "Invalid Date";
				return false;
			} else {
				unset($this->errors['date']);
				return true;
			}
		} else {
			$this->errors['date'] = "Please enter a date.";
			return false;
		}
	}

	/**
	 * create or update an object
	 * @return boolean true on success
	 */
	public function save()
	{
		if ($this->validate())
		{
			if ($this->id != null && $this->id > 0)
			{
				$this->update();
			}
			else
			{
				$this->id = $this->create();
			}

			return true;
		}

		return false;
	}

	/**
	 * Creates a new object in the database
	 * @return integer ID of the newly created object (lastInsertId)
	 */
	public function create()
	{
		// Get a db connection.
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);

		// Insert columns.
		$columns = array('date', 'num1', 'num2', 'num3', 'num4', 'num5', 'num6');

		// Prepare the insert query.
		$query
			->insert($db->quoteName('tipp'))
			->columns($db->quoteName($columns))
			->values(implode(',', array($db->quote($this->date), $db->quote($this->num1), $db->quote($this->num2), $db->quote($this->num3), $db->quote($this->num4), $db->quote($this->num5), $db->quote($this->num6))));

		// Set the query using our newly populated query object and execute it.
		$db->setQuery($query);
		$db->execute();
	}

	/**
	 * Saves the object to the database
	 */
	public function update()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->update($db->quoteName('tipp'))
			->set(array(
				$db->quoteName('date') . ' = ' . $db->quote($this->date),
				$db->quoteName('num1') . ' = ' . $db->quote($this->num1),
				$db->quoteName('num2') . ' = ' . $db->quote($this->num2),
				$db->quoteName('num3') . ' = ' . $db->quote($this->num3),
				$db->quoteName('num4') . ' = ' . $db->quote($this->num4),
				$db->quoteName('num5') . ' = ' . $db->quote($this->num5),
				$db->quoteName('num6') . ' = ' . $db->quote($this->num6)
			))
			->where($db->quoteName('id') . ' = ' . $db->quote($this->id));
		$db->setQuery($query);
		$db->execute();
	}

	/**
	 * Get an object from database
	 *
	 * @param integer $id
	 *
	 * @return object single object or null
	 */
	public static function get($id)
	{
		// Get a db connection.
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);

		$query->select($db->quoteName(array('id', 'date', 'num1', 'num2', 'num3', 'num4', 'num5', 'num6')));
		$query->from($db->quoteName('tipp'));
		$query->where($db->quoteName('id') . ' = '. $db->quote($id));

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();

        if (!empty($results)) {
            $resultObject = new Lottery($results[0]->id, $results[0]->date, $results[0]->num1, $results[0]->num2, $results[0]->num3, $results[0]->num4, $results[0]->num5, $results[0]->num6);
        } else $resultObject = null;


        return $resultObject;
	}

	/**
	 * Get an array of objects from database
	 *
	 * @param null $start
	 *
	 * @return array array of objects or empty array
	 */
	public static function getAll($start = null)
	{
		// Get a db connection.
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);

		$query->select($db->quoteName(array('id', 'date', 'num1', 'num2', 'num3', 'num4', 'num5', 'num6')));
		$query->from($db->quoteName('tipp'));
		if ($start != null)
		{
			$query->where($db->quoteName('date') . ' >= '. $db->quote($start));
		}

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();

		return $results;
	}

	/**
	 * Deletes the object from the database
	 *
	 * @param integer $id
	 */
	public static function delete($id)
	{
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->delete($db->quoteName('tipp'));
		$query->where($db->quoteName('id') . ' = ' . $db->quote($id));
		$db->setQuery($query);

		$result = $db->execute();
	}

	/**
	 * JsonSerializable implementation
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			"id"   => $this->id,
			"date" => $this->date,
			"num1" => $this->num1,
			"num2" => $this->num2,
			"num3" => $this->num3,
			"num4" => $this->num4,
			"num5" => $this->num5,
			"num6" => $this->num6
		];
	}

	/**
	 * Getter for some private attributes
	 * @return mixed $property
	 */
	public function __get($property)
	{
		if (property_exists($this, $property))
		{
			return $this->$property;
		}

		return null;
	}

	/**
	 * Setter for some private attributes
	 * @return mixed $name
	 * @return mixed $value
	 */
	public function __set($property, $value)
	{
		if (property_exists($this, $property))
		{
			$this->$property = $value;
		}
	}

}