jQuery(document).ready(function () {

    initializeLabelsArray();
    loadLotteries();

    $("#btnSearch").click(function () {
        loadFilteredLotteries();
    })
    // load and visualize data

});

function loadLotteries() {
    $.get("modules/mod_lottery/api.php?r=lotteries", function (data) {
        $("#lotteries").html(parseLotteriesTable(data));
        drawLotteryChart(data);
    });

}

function loadFilteredLotteries() {
    let start = $("#start").val();
    let date = new Date(start);

    if (date == "Invalid Date") {
        loadLotteries();
    }
    else {
        $.get("modules/mod_lottery/api.php?r=lotteries/start/" + start, function (data) {
            $("#lotteries").html(parseLotteriesTable(data));
            drawLotteryChart(data);
        })
    }
}

function parseLotteriesTable(data) {
    let tmp = "";

    data.forEach(function (d) {
        tmp += '<tr>';
        tmp += '<td>' + d.date + '</td>';
        tmp += '<td>' + d.num1 + '</td>';
        tmp += '<td>' + d.num2 + '</td>';
        tmp += '<td>' + d.num3 + '</td>';
        tmp += '<td>' + d.num4 + '</td>';
        tmp += '<td>' + d.num5 + '</td>';
        tmp += '<td>' + d.num6 + '</td>';
        tmp += '</td>';
        tmp += '</tr>';
    });

    return tmp;
}

// store reference to the chart, required for clearing
var barChart = null;

let labels = null;

/**
 * Draws the lottery chart (into the page-element with the id 'lotteryChart'
 * @param data The data
 */
function drawLotteryChart(data) {

    // clear old chart
    if (barChart != null) {
        barChart.destroy();
    }

    let ctx = document.getElementById("lotteryChart").getContext('2d');

    let dataset = generateDataset(data);

    barChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [dataset]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

/**
 * Returns the formatted (for frequency) dataset
 * @param data Data retrieved from the REST api
 * @returns {{label: string, data: Array, borderColor: string, backgroundColor: string, fill: boolean, lineTension: number}}
 */
function generateDataset(data) {

    let d = [];
    for (let i = 0; i < 45; i++) {
        d[i] = 0;
    }

    data.forEach(e => {
        d[e.num1 - 1]++;
        d[e.num2 - 1]++;
        d[e.num3 - 1]++;
        d[e.num4 - 1]++;
        d[e.num5 - 1]++;
        d[e.num6 - 1]++;
    });

    return {
        label: 'Häufigkeit',
        data: d,
        borderColor: "rgb(71, 20, 255)",
        backgroundColor: "rgb(71, 20, 255)",
        fill: false,
        lineTension: 0.1
    }
}

/**
 * Initializes the labels array fo an array of the numbers 1 to 45
 */
function initializeLabelsArray() {
    let l = [];

    for (let i = 0; i < 45; i++) {
        l[i] = i + 1;
    }

    labels = l;
}